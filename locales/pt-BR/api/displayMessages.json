{
    "httpMethodNotAllowed": "O método de HTTP \"{{method}}\" não é aceito nessa requisição",
    "endpointNotFound": "Não conseguimos encontrar essa requisição",
    "errorOccured": "Um erro ocorreu",
    "missingParameter": "Faltam {{parameter}}",
    "invalidParameter": "{{parameter}} inválido",
    "invalidParameterType": "{{parameter}} deve ser {{type}}",
    "parameterTooLong": "{{parameter}} não pode ultrapassar {{maxLen}} caracteres",
    "parameterTooShort": "{{parameter}} deve ter no mínimo {{minLen}} caracteres",
    "parameterNotBetween": "{{parameter}} deve ser um valor entre {{min}} e {{max}}",
    "discordAccountNotLinked": "É necessário conectar uma conta do Discord",
    "notLoggedIn": "Você deve estar logado para realizar essa requisição",
    "alreadyLoggedIn": "Você já está logado",
    "invalidUsernameCharacters": "O nome de usuário deve conter apenas caracteres alfanuméricos",
    "usernameTaken": "Esse nome de usuário já é usado por alguém",
    "passwordMismatch": "As senhas providas não batem",
    "changePasswordSuccess": "Senha alterada com sucesso",
    "resourceNotFound": "Um {{resource}} com essa {{property}} não pôde ser encontrado",
    "resourceExists": "Um {{resource}} com essa {{property}} já existe",
    "premiumFeature": "Você precisa de uma subscrição ativa para usar o seguinte {{feature}}",
    "userNotFound": "Não foi possível encontrar esse usuário",
    "fileTooLarge": "Esse arquivo é muito grande, o tamanho máximo permitido é {{size}}",
    "invalidFileFormat": "Tipo inválido de arquivo",
    "bodyTooLarge": "O corpo provido é mais longo do que o máximo permitido",
    "bodyMalformed": "O corpo provido está formatado de maneira inadequada",
    "authenticationApi": {
        "accountBanned": "A sua conta foi banida pelo seguinte motivo: {{reason}}",
        "invalidForgotPasswordToken": "O token provido é inválido ou já expirou.",
        "passwordResetLinkSent": "Enviamos um link de reset de senha para o seu privado do Discord. Esse link expira em 24 horas.",
        "suspectedMultiAccounting": "Parece que você tem mais de uma conta, e isso vai contra nossos termos de serviço... Contate o nosso suporte se você acredita que isso é um erro.",
        "noReasonProvided": "Nenhum motivo foi provido",
        "invalidInviteCode": "Esse convite não existe ou já foi utilizado",
        "refreshTokenMismatch": "O token atualizado não corresponde ao token provido",
        "passwordResetRequired": "Você precisa resetar sua senha",
        "notBetaUser": "Você deve ser um usuário beta para acessar esse website",
        "mfaCodeRequired": "Um código de autenticação multi-fator é necessário para acessar esse conteúdo",
        "failedCaptcha": "Ops, o captcha falhou, tente novamente"
    },
    "collectionsApi": {
        "invalidPublicityType": "A publicidade da coleção deve ser \"public\" ou \"private\", não é possível deixá-la sem valor",
        "collectionNotEmpty": "A coleção deve estar vazia antes de ser deletada."
    },
    "discordApi": {
        "accountTooYoung": "Sua conta de Discord é muito nova para ser conectada",
        "accountNotFound": "Não podemos lhe autenticar com o Discord, uma vez que não há nenhuma conta do serviço associada com essa conta de Discord."
    },
    "domainGroupsApi": {
        "usersMustBeArray": "A lista de usuários deve conter uma combinação de UUIDs de usuários",
        "noValidUsers": "A lista de usuários deve ter no mínimo 1 UUID de usuário válido",
        "invalidUserIds": "IDs inválidos de usuários foram providos.",
        "groupInUse": "Esse grupo de acesso já é usado por um domínio, defina ele para outro domínio antes de deletá-lo."
    },
    "domainsApi": {
        "invalidTld": "Infelizmente não estamos aceitando domínios com esse TLD no momento",
        "deletedDomain": "Domínio deletado com sucesso"
    },
    "imagesApi": {
        "futureTimestamp": "A chave requisitada deve ser um valor de tempo no futuro",
        "unauthenticated": "Você deve estar autenticado para fazer esse tipo de pedido",
        "invalidUploadKey": "Chave de upload inválida. Você tem certeza que já gerou uma chave?",
        "subscriptionMaxFileSize": "O arquivo ultrapassa o valor máximo de upload do seu plano, que é {{size}}",
        "subscriptionMaxStorage": "Você excedeu o seu limite de armazenamento. Delete alguns arquivos ou nos ajude a continuar online fazendo o upgrade de seu plano.",
        "missingUploadPreferences": "Não encontramos um domínio válido, atualize as suas preferências na página de preferências de upload.",
        "userBanned": "Você está banido. Contate o nosso suporte se você acredita que isso é um erro.",
        "unrecognisedIp": "Endereço de IP não reconhecido ou válido. Realize o login novamente e tente mais tarde.",
        "missingExtension": "O arquivo deve ter uma extensão de arquivo válida",
        "premiumFileType": "Você precisa de um upgrade de plano para realizar o upload desse tipo de arquivo",
        "spamUploadBan": "Você foi banido por fazer o upload de imagens muito rapidamente de maneira a indicar spam, contate o suporte para apelar essa decisão",
        "noDeletePermissions": "Você não tem permissão para deletar imagens de outros usuários",
        "wipeInProgress": "Suas imagens já estão sendo limpas ({{percentage}}% concluído)",
        "noImagesInCollection": "Não existem imagens na coleção provida",
        "noValidImageIds": "Não foram providos IDs válidos de imagem",
        "noImagesTo": "Você não tem imagens para {{action}}",
        "archiveInProgress": "Você já tem um arquivo sendo processado",
        "archiveAlreadyRequested": "Você já requisitou um arquivo de imagens nos últimos 7 dias",
        "reportOwnImage": "Você não pode reportar suas próprias imagens",
        "imageAlreadyReported": "Você já reportou",
        "cannotSetFileAsShowcase": "Você só pode definir vídeos ou imagens como imagem de exibição"
    },
    "mailAccountsApi": {
        "invalidDomain": "Invalid domain within email",
        "maxAccounts": "Você já chegou ao máximo de contas de email que você pode ter simultaneamente",
        "maxAliases": "Você já chegou ao máximo de endereços de email que você pode ter simultaneamente",
        "emailInUse": "Esse endereço de email já está em uso",
        "aliasExists": "Um endereço de email com esse nome já existe",
        "invalidCharacters": "Você tem caracteres invalidos no seu endereço de email"
    },
    "notificationsApi": {
        "alreadyMarkedAsRead": "Essa notificação já foi marcada como lida"
    },
    "pastesApi": {
        "unsupportedLang": "O tipo de sintaxe de linguagem provido não é suportado",
        "unsupportedType": "O tipo de colagem provido não é suportado",
        "privateAndPasswordProtected": "Uma colagem não pode ser privada e protegida por senha ao mesmo tempo",
        "successfullyWiped": "Limpamos com sucesso o total de {{count}} colagens",
        "passwordRequired": "É necessário uma senha para decriptar essa colagem",
        "incorrectPassword": "Senha incorreta",
        "notOwnPaste": "Você só pode deletar as suas próprias colagens"
    },
    "shortenUrlApi": {
        "invalidProtocol": "Protocolo inválido de URL, ele deve usar um protocolo HTTP ou HTTPS",
        "invalidIDCharacters": "Os IDs apenas podem conter caracteres alfanuméricos",
        "selfRedirect": "Uma URL encurtada não pode ser direcionada a ela mesma",
        "successfullyWiped": "Limpamos com sucesso o total de {{count}} URLs encurtadas",
        "notOwnShortenedUrl": "Você só pode deletar as suas próprias URLs encurtadas"
    },
    "subscriptionsApi": {
        "notSelected": "Você não selecionou um grupo para subscrição",
        "activeSubscription": "Você já tem uma subscrição ativa",
        "groupMismatch": "O usuário provido já está subscrito ao plano {{groupName}}, então não é possível presenteá-lo com esse plano",
        "selfGift": "Você não pode presentear uma subscrição a você mesmo, adicione meses a sua subscrição acessando a página de gerenciamento de subscrições",
        "userNotGiftable": "Você não pode presentear uma subscrição para esse usuário agora, peça para que eles façam login primeiro",
        "noSubscriptionAddMonths": "Você não tem uma subscrição ativa para adicionar meses.",
        "noSubscriptionCancel": "Você não tem uma subscrição ativa para cancelar",
        "noSubscriptionResume": "Você não tem uma subscrição ativa para continuar o uso",
        "pendingPlanChange": "Você já tem uma mudança de plano pendente",
        "noDefaultPaymentMethod": "Você deve ter no mínimo 1 método de pagamento adicionado para se subscrever novamente",
        "cannotPreviewFree": "Você não pode visualizar a prévia de um plano gratuito, mas pode cancelar a sua subscrição",
        "cannotPreviewNoSubscription": "Você não pode visualizar a prévia de um plano sem um outro plano ativo",
        "cannotPreviewSamePlan": "Você não pode visualizar uma prévia de um plano já adquirido",
        "cannotChangeAddedMonths": "Você não pode trocar de plano agora porquê você adicionou meses a sua subscrição atual. Você poderá mudar seu plano em  {{date}}.",
        "cannotChangeGifted": "Você não pode trocar de plano agora porquê você está usando uma subscrição presenteada. Você poderá mudar seu plano em {{date}}."
    },
    "themesApi": {
        "noDeletePermissions": "Você não tem permissão para deletar esse tema"
    },
    "userApi": {
        "privateProfile": "O perfil desse usuário é privado",
        "noValidProperties": "Faltam atributos válidos em seu pedido",
        "multiLevelSubdomains": "Sub-domínios de multinível não são aceitos, use - ao invés de .",
        "subdomainsNotAllowed": "Sub-domínios não são aceitos nesse domínio.",
        "maxDomainCount": "Você só pode selecionar {{count}} domínios ao mesmo tempo",
        "minDomainCount": "Você precisa selecionar no mínimo 2 domínios",
        "invalidDomain": "{{domain}} não é um domínio válido",
        "invalidSubdomainDomain": "Sub-domínios não são aceitos no domínio {{domain}}",
        "autoChanceZero": "Os valores providos resultam nas chances automáticas sendo 0%.",
        "randomChanceNot100": "As chances somadas de domínios randômicos devem resultar em 100.",
        "invalidUrl": "{{property}} deve ser uma URL com protocolo HTTP ou HTTPS válido",
        "invalidEmbedFields": "Não é possível usar anexos do Discord vazios, preencha alguns campos ou desative os anexos.",
        "noAuthorOrTitle": "É necessário um título ou autor para que o Discord reconheça o seu anexo de maneira correta.",
        "invalidTimeformat": "o formato de tempo deve ser 12h ou 24h.",
        "cropAreaExceedsSize": "A área de corte não pode ser maior que a área da imagem",
        "usernameChangeUnavailable": "Você já trocou seu nome de usuário nos últimos 7 dias",
        "unsupportedLanguage": "Linguagem não suportada submetida",
        "invalidConfigType": "Tipo inválido de configuração submetida",
        "unprocessableGif": "Não conseguimos processar esse GIF",
        "fileAgeNegative": "File age cannot be a negative value",
        "fileAgeTooBig": "File age cannot be longer than 12 months"
    },
    "userApis": {
        "MFAApi": {
            "mfaNotInitalised": "A autenticação de multi-fatores não pode ser inicializada.",
            "mfaNotEnabled": "Você não tem uma autenticação de multi-fatores ativa."
        },
        "paymentApi": {
            "atLeastOnePaymentMethodRequired": "Você precisa ter no mínimo um método de pagamento definido enquanto tem uma subscrição ativa.",
            "defaultPaymentMethod": "Você não pode deletar um método de pagamento definido como padrão.",
            "amountDecimalPlaces": "A quantia não pode ter mais de duas casas decimais",
            "amountTooSmall": "A quantia não pode ser menor que {{amount}}",
            "amountTooLarge": "A quantia não pode ser maior que {{amount}}"
        },
        "uploadPresetsApi": {
            "missingParameters": "Faltam parâmetros na sua configuração.",
            "maxPresets": "Você só pode ter no máximo 10 predefinições de upload."
        }
    }
}
